import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import { Meteor } from 'meteor/meteor';
import moment from 'moment'

import template from './mobile.html';
import { Orders } from '../../../api/orders';

class Current {
    constructor($scope, $reactive, $state, $ionicModal) {
        'ngInject';

        $reactive(this).attach($scope);

        this.subscribe("orders");
        this.state = $state;

        this.select = '';

        this.latitude = undefined;
        this.longitude = undefined;

        this.selectedReason = 0;
        this.reasons = [];

        this.modal = undefined;

        //Get Reasons
        HTTP.get("https://euroclean.be/temconet/api/webuser/?action=getpossiblereasons&username=", (error, result) => {
            if (!error) {
                this.reasons = result.data;
            }
        });

        $ionicModal.fromTemplateUrl('reasons.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then((modal)=>{
            this.modal = modal;
        });

        this.openModal = ()=> {
            this.modal.show();
        };
        this.closeModal = ()=> {
            this.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', ()=> {
            this.modal.remove();
        });

        this.getLocation = ()=> {

            navigator.geolocation.getCurrentPosition
            (this.onMapSuccess, this.onMapError, { enableHighAccuracy: true });
        };

        this.addStatus = (status) => {
            Orders.update(this.getReactively('select')._id, {
                $set: {status: status},
                $addToSet: {timings: {status: status, timestamp: moment().unix()}}
            });
            this.select.status = status;

            //Get Location
            this.getLocation();

        };

        // Success callback for get geo coordinates

        this.onMapSuccess = (position)=> {

            this.latitude = position.coords.latitude;
            this.longitude = position.coords.longitude;

            //Send status to API
            let username = Meteor.user().profile.id;
            let orderID = this.getReactively('select')._id;
            let order = Orders.findOne(orderID);

            //Check to get last status action
            let timeslots = _.sortBy(order.timeslots, (timeslot) => {
                return timeslot.TS_StartTime;
            });

            if (timeslots.length > 0) {
                timeslots = timeslots.reverse();
            }

            //Get Type 7
            let lastStatus = timeslots.length > 0 ? timeslots[0] : null;
            //console.log(lastStatus);

            let reqID = order.RQ_ID;
            let ticketID = order.RQ_ID;
            let execDate = moment().format("YYYY-MM-DD");
            let startTime = null;
            let stopTime = null;
            let typeNumber = 1; //type = 3 possible values, 1: Working Hours, (2: Travel Time), 3: interrupt (pause)
            let reason = "";
            switch (this.select.status) {
                case "start":
                    startTime = moment().format("YYYY-MM-DDTHH:mm:ss");
                    break;
                case "hold":
                    //Should update recent start request
                    startTime = moment(lastStatus.TS_StartTime, 'MMM DD YYYY HH:mm:ss0A').format("YYYY-MM-DDTHH:mm:ss");
                    ticketID = order.RQ_ID;
                    stopTime = moment().format("YYYY-MM-DDTHH:mm:ss");
                    reason = "&reason=" + this.selectedReason;
                    typeNumber = 4;
                    break;
                case "stop":
                    //Should update recent start request
                    startTime = moment(lastStatus.TS_StartTime, 'MMM DD YYYY HH:mm:ss0A').format("YYYY-MM-DDTHH:mm:ss");
                    ticketID = order.RQ_ID;
                    stopTime = moment().format("YYYY-MM-DDTHH:mm:ss");
                    reason = "&reason=0";
                    break;
                case "pauze":
                    startTime = moment(lastStatus.TS_StartTime, 'MMM DD YYYY HH:mm:ss0A').format("YYYY-MM-DDTHH:mm:ss");
                    ticketID = order.RQ_ID;
                    stopTime = moment().format("YYYY-MM-DDTHH:mm:ss");
                    let url = "https://euroclean.be/temconet/api/webuser/?username=" + username + "&action=regtimeslot&requestID=" + reqID +
                        "&executionDate=" + execDate + "&startTime=" + startTime + "&stopTime=" + stopTime + "&type=" + typeNumber +
                        "&ticketID=" + ticketID +"&lng=" + this.longitude + "&lat=" + this.latitude;
                    console.log(url);
                    HTTP.get(url, (error, result) => {
                        if (!error) {
                            console.log(result);
                        }
                    });

                    //Reset for pauze request
                    ticketID = order.RQ_ID;
                    startTime = moment(lastStatus.TS_StartTime, 'MMM DD YYYY HH:mm:ss0A').format("YYYY-MM-DDTHH:mm:ss");
                    stopTime = null;
                    typeNumber = 3;
                    break;
            }

            let url = "https://euroclean.be/temconet/api/webuser/?username=" + username + "&action=regtimeslot&requestID=" + reqID +
                "&executionDate=" + execDate + "&startTime=" + startTime + "&stopTime=" + stopTime + "&type=" + typeNumber +
                "&ticketID=" + ticketID +"&lng=" + this.longitude + "&lat=" + this.latitude + reason;
            console.log(url);
            HTTP.get(url, (error, result) => {
                if (!error) {
                    console.log("Adding timeslot for " + this.getReactively('select')._id);
                    console.log(result);
                    if(this.select.status === 'start' || this.select.status === 'pauze') {
                        Orders.update(this.getReactively('select')._id, {
                            $addToSet: {
                                timeslots: {
                                    TS_ID: result.data.id,
                                    TS_StartTime: moment(startTime).format("MMM DD YYYY HH:mm:ss0A"),
                                    TS_StopTime: stopTime ? moment(stopTime).format("MMM DD YYYY HH:mm:ss0A") : '',
                                    TS_Type: typeNumber,
                                    TS_ActionID: result.data.actionID
                                }
                            }
                        });
                    }else{
                        //update
                        Meteor.call('updateTimeslot', this.getReactively('select')._id, moment(startTime).format("MMM DD YYYY HH:mm:ss0A"), moment(stopTime).format("MMM DD YYYY HH:mm:ss0A"), typeNumber, result.data.actionID, this.selectedReason);
                        this.selectedReason = 0;
                    }
                }
            });

        };

        // Error callback

        this.onMapError = (error)=> {
            console.log('code: ' + error.code + '\n' +
                'message: ' + error.message + '\n');
        };

        this.helpers({
            orders() {
                let orders = Orders.find({}, {sort: {"RQ_ExecutionDate": 1}}).fetch();

                //group by date
                orders = _.groupBy(orders, (order) => {
                    let date = order.RQ_ExecutionDate.substr(0, order.RQ_ExecutionDate.length - 15);
                    return moment(date).format("MM-DD-YYYY");
                });

                return orders[moment().format("MM-DD-YYYY")];
            },
            getReasons() {
                return this.getReactively("reasons");
            }

        });

    }

    selectTo(order) {
        console.log(order);
        this.select = order;
    }

    start() {
        this.addStatus("start");
    }

    pauze() {
        this.addStatus("pauze");
    }

    stop() {
        this.addStatus("stop");
    }

    hold() {
        this.addStatus("hold");
        this.closeModal();
    }

    openHold() {
        this.openModal();
    }

}

const name = 'current';

// create a module
export default angular.module(name, [
    angularMeteor,
    uiRouter
]).component(name, {
    template,
    controllerAs: name,
    controller: Current
})
    .config(config);

function config($stateProvider) {
    'ngInject';
    $stateProvider
        .state('current', {
            url: '/current',
            template: '<current></current>',
            resolve: {
                currentUser($q) {
                    if (Meteor.userId() === null) {
                        return $q.reject('AUTH_REQUIRED');
                    } else {
                        return $q.resolve();
                    }
                }
            }
        });
}