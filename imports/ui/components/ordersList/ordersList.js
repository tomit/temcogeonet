import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import { Meteor } from 'meteor/meteor';
import moment from 'moment'

import template from './mobile.html';
import { Orders } from '../../../api/orders';

class OrdersList {
    constructor($scope, $reactive, $state) {
        'ngInject';

        $reactive(this).attach($scope);

        this.subscribe("orders");
        this.state = $state;

        this.helpers ({
            orders() {
                let orders = Orders.find({}, {sort: {"RQ_ExecutionDate" : 1}}).fetch();

                //group by date
                orders = _.groupBy(orders, (order) => {
                    let date = order.RQ_ExecutionDate.substr(0, order.RQ_ExecutionDate.length - 15);
                    return moment(date).format("DD/MM/YYYY");
                });

                return orders;
            }
        });

    }

}

const name = 'ordersList';

// create a module
export default angular.module(name, [
    angularMeteor,
    uiRouter
]).component(name, {
    template,
    controllerAs: name,
    controller: OrdersList
})
    .config(config);

function config($stateProvider) {
    'ngInject';
    $stateProvider
        .state('orders', {
            url: '/orders',
            template: '<orders-list></orders-list>',
            resolve: {
                currentUser($q) {
                    if (Meteor.userId() === null) {
                        return $q.reject('AUTH_REQUIRED');
                    } else {
                        return $q.resolve();
                    }
                }
        }});
}