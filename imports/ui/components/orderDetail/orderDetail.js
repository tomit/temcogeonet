import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import { Meteor } from 'meteor/meteor';
import moment from 'moment'

import template from './mobile.html';
import { Orders } from '../../../api/orders';

class OrderDetail {
    constructor($stateParams, $scope, $reactive, $ionicLoading, $state, $ionicModal) {
        'ngInject';

        $reactive(this).attach($scope);

        this.subscribe("order", () => [$stateParams.orderId]);

        this.orderId = $stateParams.orderId;

        $ionicModal.fromTemplateUrl('comment.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then((modal)=>{
            this.modal = modal;
        });

        this.openModal = (action)=> {
            this.modal.show();
            this.selectedId = action.AC_ID;
            this.selectedComment = action.AC_Comment;
        };
        this.closeModal = ()=> {
            this.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', ()=> {
            this.modal.remove();
        });

        this.helpers ({
            order() {
                return Orders.findOne({owner: Meteor.userId(), RQ_ID: $stateParams.orderId}, {limit: 1});
            }
        });
    }

    comment() {
        let url = "https://euroclean.be/temconet/api/webuser/?username=&action=commentaction&id=" + this.selectedId +
            "&comment=" + this.selectedComment;
        console.log(url);
        HTTP.get(url, (error, result) => {
            if (!error) {
                this.closeModal();
                console.log(result);
                Meteor.call('updateActionComment', this.orderId, this.selectedId, this.selectedComment);
            }
        });

    }

    customDate(date) {
        if(date == "") {
            return "";
        }
        return moment(date, 'MMM DD YYYY HH:mm:ss0A').format('DD-MM-YYYY HH:mm');
    }

}

const name = 'orderDetail';

// create a module
export default angular.module(name, [
    angularMeteor,
    uiRouter
]).component(name, {
    template,
    controllerAs: name,
    controller: OrderDetail
})
    .config(config);

function config($stateProvider) {
    'ngInject';
    $stateProvider
        .state('order', {
            url: '/orders/:orderId',
            template: '<order-detail></order-detail>',
            resolve: {
                currentUser($q) {
                    if (Meteor.userId() === null) {
                        return $q.reject('AUTH_REQUIRED');
                    } else {
                        return $q.resolve();
                    }
                }
        }});
}