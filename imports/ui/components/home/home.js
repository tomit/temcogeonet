import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import moment from 'moment'

import { Meteor } from 'meteor/meteor';


import template from './mobile.html';
import { Workdays } from '../../../api/workdays';

class Home {
    constructor($rootScope, $scope, $reactive, $state, $timeout, $ionicSideMenuDelegate) {
        'ngInject';

        $reactive(this).attach($scope);

        $ionicSideMenuDelegate.canDragContent(false);

        this.state = $state;

        this.subscribe("workdays");

        this.helpers({
            status: () => {
                if(Meteor.user()){
                    let workday = Workdays.findOne({
                        username: Meteor.user().profile.id,
                        date: moment().format("DD-MM-YYYY")
                    }, {
                        sort: {
                            timestamp: -1
                        }
                    });

                    this.status = workday ? workday.working : false;
                    return this.status;
                }else{
                    return false;
                }
            }
        });

        this.getLocation = ()=> {
            navigator.geolocation.getCurrentPosition
            (this.onMapSuccess, this.onMapError, { enableHighAccuracy: true });
        };

        this.onMapSuccess = (position)=> {

            let latitude = position.coords.latitude;
            let longitude = position.coords.longitude;

            this.sendTimeslot(latitude, longitude);

        };

        this.onMapError = (error)=> {
            console.log('code: ' + error.code + '\n' +
                'message: ' + error.message + '\n');
        };

        this.sendTimeslot = (lat, lng)=> {

            let username = Meteor.user().profile.id;
            let execDate = moment().format("YYYY-MM-DD");
            let startTime = this.status ? moment().format("YYYY-MM-DDTHH:mm:ss") : null;
            let stopTime = this.status ? null : moment().format("YYYY-MM-DDTHH:mm:ss");
            let typeNumber = 2; //type = 3 possible values, 1: Working Hours, (2: Travel Time), 3: interrupt (pause)

            let url = "https://euroclean.be/temconet/api/webuser/?username=" + username +
                "&action=regtimeslotworkday&executionDate=" + execDate +
                "&startTime=" + startTime + "&stopTime=" + stopTime + "&type=" + typeNumber +
                "&lng=" + lng + "&lat=" + lat ;
            console.log(url);
            HTTP.get(url, (error, result) => {
                if (!error) {
                    console.log(result);
                    Meteor.call('updateActionComment', this.orderId, this.selectedId, this.selectedComment);
                }
            });
        };

        this.working = (status) => {
            $rootScope.isWorking = status;
            this.status = status;

            Workdays.insert({
                timestamp: moment().unix(),
                working: status,
                user: Meteor.userId(),
                username: Meteor.user().profile.id,
                date: moment().format("DD-MM-YYYY")
            });
            this.getLocation();
        };

        this.doRefresh = () => {
            this.fetchOrdersFromRemote(1);
            return true;
        };

        this.fetchOrdersFromRemote = (renew) => {

            if (Meteor.status().status === "connected") {
                console.log("getting the orders");
                Meteor.call("getOrders", function (err, res) {
                    if (renew) {
                        $scope.$broadcast('scroll.refreshComplete');
                    }
                });
            }

            if (renew) {
                $scope.$broadcast('scroll.refreshComplete');
                return $timeout(function () {
                    $state.go('.', {}, {reload: true});
                }, 1000);
            }
        };

        this.fetchOrdersFromRemote(1);

    }

}

const name = 'home';

// create a module
export default angular.module(name, [
    angularMeteor,
    uiRouter
]).component(name, {
    template,
    controllerAs: name,
    controller: Home
})
    .config(config);

function config($stateProvider) {
    'ngInject';
    $stateProvider
        .state('home', {
            url: '/',
            template: '<home></home>',
            resolve: {
                currentUser($q) {
                    if (Meteor.userId() === null) {
                        return $q.reject('AUTH_REQUIRED');
                    } else {
                        return $q.resolve();
                    }
                }
            }
        });
}