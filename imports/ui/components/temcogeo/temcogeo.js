import angular from 'angular';
import angularMeteor from 'angular-meteor';
import ngSanitize from 'angular-sanitize';
import ngAnimate from 'angular-animate';
import uiRouter from 'angular-ui-router';
import 'ionic-sdk/release/js/ionic';
import 'ionic-sdk/release/js/ionic-angular';
import 'ionic-sdk/release/css/ionic.css';

import moment from 'moment'

import { Meteor } from 'meteor/meteor';
import { Php } from '../../../scripts/php.js';

import { name as Home } from '../home/home';
import { name as Login } from '../login/login';
import { name as Footer } from '../footer/footer';
import { name as OrdersList } from '../ordersList/ordersList';
import { name as OrderDetail } from '../orderDetail/orderDetail';
import { name as Current } from '../current/current';


import template from './mobile.html';

class Temcogeo {
    constructor($scope, $rootScope, $reactive, $ionicLoading, $ionicBackdrop, $ionicConfig) {
        'ngInject';

        $reactive(this).attach($scope);
        this.$rootScope = $rootScope;

        this.status = Meteor.status().status;
        $ionicConfig.tabs.position('bottom');

        this.helpers({
            connected: () => {
                let status = this.getReactively("status");
                $ionicBackdrop.release();
                if (status == 3) {
                    $ionicBackdrop.retain();
                    $ionicLoading.show({
                        template: '<p>Connection error</p><ion-spinner></ion-spinner>'
                    });
                } else if (status == "2") {
                    $ionicBackdrop.retain();
                    $ionicLoading.show({
                        template: '<p>Reconnecting</p><ion-spinner></ion-spinner>'
                    });
                } else {
                    $ionicBackdrop.release();
                    $ionicLoading.hide();
                }
                return status == 1;

            }
        });

    }
}

const name = 'temcogeo';

// create a module
export default angular.module(name, [
    angularMeteor,
    ngSanitize,
    ngAnimate,
    uiRouter,
    'ionic',
    Login,
    Footer,
    Home,
    OrdersList,
    OrderDetail,
    Current
]).component(name, {
    template,
    controllerAs: name,
    controller: Temcogeo
})
    .config(config)

    //Filters:

    .filter('filterHtml', function($sce) {
        return function(val) {
            let html = $sce.trustAsHtml(val);
            html = Php.stripslashes(html);
            return html;
        };
    })
    .filter('stripSlashes', function() {
        return function(htm) {
            return Php.stripslashes(htm);
        };
    })
    .filter('stripTags', function() {
        return function(htm) {
            return Php.strip_tags(htm);
        };
    })
    .run(run);

function run ($rootScope, $state, $meteor, $ionicHistory) {
    'ngInject';
    // global functions

    $rootScope.logMeOut = function () {
        let userId = Meteor.userId();

        $meteor.logout().then(function () {
            Meteor.call('removeOrders', userId);
            Meteor.call('removeUser', userId);
            $state.go('login');
        });
    };

    $rootScope.goBackOnePage = () => {
        $ionicHistory.goBack();
    };

    $rootScope.gotoMainTab = function (args) {
        $state.go(args, {}, {
            reload: true,
            inherit: false,
            notify: true
        });
    };

    $rootScope.userConnected = function () {
        return Meteor.status().status === "connected";

    };
    $rootScope.checkUserLoggedOn = function () {
        return !!Meteor.userId();

    };

    $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {

        console.log(error);console.log("in statechange");
        if (error === 'AUTH_REQUIRED') {
            $state.go('login');
        }
        else if (error === 'LOGIN_NOT_OK') {
            $state.go('home');
        }
        else if (error === 'LOGGED_IN') {
            $state.go('home');
        }

    });

    $rootScope.isWorking = false;

    $meteor.autorun($rootScope, function() {

        //Todo: does thix also uses gps track?
        //Session.set('pos',Geolocation.latLng());

        if (Meteor.status().status === "connected") {
            Session.set('connected', 1);
        }
        else if (Meteor.status().status === "connecting") {
            //console.log("reconnecting");
            Session.set('connected', 2);
        }
        else {
            //console.log("disconnected");

            Session.set('connected', 3);
        }
    });
}


function config($locationProvider, $urlRouterProvider) {
    'ngInject';

    $locationProvider.html5Mode(true);

    $urlRouterProvider.otherwise('/');

}
