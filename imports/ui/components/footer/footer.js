import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import { Meteor } from 'meteor/meteor';


import template from './mobile.html';

class Footer {
    constructor($rootScope, $scope, $reactive, $state) {
        'ngInject';

        $reactive(this).attach($scope);
        this.$rootScope = $rootScope;
    }

}

const name = 'footer';

// create a module
export default angular.module(name, [
    angularMeteor,
    uiRouter
]).component(name, {
    template,
    controllerAs: name,
    controller: Footer
});