import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import { Meteor } from 'meteor/meteor';


import template from './mobile.html';
//import { Orders } from '../../../api/orders';

class Login {
    constructor($scope, $reactive, $state, $ionicPopup, $ionicLoading) {
        'ngInject';
        $reactive(this).attach($scope);

        //let userLanguage = window.navigator.userLanguage || window.navigator.language;
        let userLanguage = "BE_NL";

        Meteor.call('forgotPasswordLink', userLanguage, function (err, result) {
            if (!err) {
                this.passwordUrl = result.passwordurl;
                this.siteUrl = result.siteurl;
            }
        });

        function errorLogOn() {
            var alertPopup = $ionicPopup.alert({
                title: 'Authentication',
                template: 'Error login'
            });
            alertPopup.then(function (res) {
                $('input[name=password]').val('');
            });
        };

        this.helpers({
            connected: () => {
                return (Meteor.status().status === "connected");
            }
        });

        this.visitWebsite = function () {
            //window.open(this.siteUrl, '_system');
        };

        this.login = function () {
            this.submitted = true;
            email = $('input[name=email]').val();
            password = $('input[name=password]').val();
            $ionicLoading.show({
                template: '<p>Checking your profile ... </p><ion-spinner></ion-spinner>'
            });
            Meteor.call('validateLogin', email, password, function (error, response) {
                if (error) {
                    $ionicLoading.hide();
                    errorLogOn();
                } else {
                    Meteor.loginWithPassword(email, password, function (err, result) {
                        if (!err) {
                            $ionicLoading.hide();
                            $state.go('home');
                        } else {
                            $ionicLoading.hide();
                            errorLogOn();
                        }
                    });
                }
            });
        }
    }
}

const name = 'login';

// create a module
export default angular.module(name, [
    angularMeteor,
    uiRouter
]).component(name, {
    template,
    controllerAs: name,
    controller: Login
})
    .config(config);

function config($stateProvider) {
    'ngInject';
    $stateProvider
        .state('login', {
            url: '/login',
            template: '<login></login>',
            resolve: {
                currentUser($q) {
                    if (Meteor.userId() === null) {
                        return $q.resolve;
                    } else {
                        return $q.reject('LOGGED_IN');
                    }
                }
            }
        });
}