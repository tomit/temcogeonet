import { Meteor } from 'meteor/meteor';

if (Meteor.isServer) {
    Meteor.methods({
        removeUser:  (userId) => {

            Meteor.users.remove({
                _id: userId
            });
        },
        validateLogin: function (email, password) {

            //TODO: get avatar?
            url = "https://euroclean.be/temconet/site/api/webuser/";

            let result = HTTP.post(url, {
                data: {
                    username: email,
                    password: password,
                    action: 'login'
                }
            });
            //console.log(result);
            if ((typeof result !== "undefined" && result !== null ? result.data : void 0) != null) {
                result = result.data;
                doc = {};
                doc.email = email;
                doc.password = password;

                if (result.id != null) {
                    user = Meteor.users.findOne({
                        "emails.0.address": email
                    });

                    result.status = 'ok';

                    if (typeof user !== undefined && user != null) {

                        if (user.passwordChange != undefined && user.passwordChange != null) {
                            Accounts.setPassword(user._id, user.passwordChange);
                            doc.passwordChange = null;
                        }

                        //Meteor.call("initCampaigns", user.profile.id);

                        doc.profile = {
                            firstName: result.firstname,
                            lastName: result.lastname,
                            id: result.id,
                            language: result.language
                        };

                        Meteor.users.update(user._id, {
                            $set: doc
                        });

                    } else {
                        doc.username = email;

                        doc.profile = {
                            firstName: result.firstname,
                            lastName: result.lastname,
                            id: result.id,
                            language: result.language
                        };

                        Accounts.createUser(doc);
                    }
                } else {
                    result.status = "Authentication failed";
                }
                if (result.status != null) {
                    return result.status;
                } else {
                    throw new Meteor.Error("Acces denied!");
                }
            } else {
                throw new Meteor.Error("Acces denied!");
            }
        }
    });
}

if (Meteor.isServer) {
    Meteor.publish('users', function () {
        return Meteor.users.find({}, {
            fields: {
                emails: 1,
                profile: 1
            }
        });
    });
}
