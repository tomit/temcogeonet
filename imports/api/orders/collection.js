import { Mongo } from 'meteor/mongo';

export const Orders = new Meteor.Collection('orders');

Orders.deny({
    insert() {
        return true;
    },
    update() {
        return false;
    },
    remove() {
        return true;
    }
});

Orders.allow({
    update() {
        return true;
    }
});

Meteor.startup(() => {
    if(Meteor.isServer) {
        // Global API configuration
        let Api = new Restivus({
            useDefaultAuth: false,
            apiPath: 'api/',
            prettyJson: true,
            enableCors: true

        });

        // Generates: GET, POST on /api/items and GET, PUT, DELETE on
        // /api/items/:id for the Items collection
        Api.addCollection(Orders,
            {
                routeOptions: {
                    authRequired: false
                },
                excludedEndpoints: [],
                endpoints: {
                    get: {
                        authRequired: false,
                        action: function() {
                            let order = Orders.findOne({RQ_ID: this.urlParams.id});

                            if( order ) {
                                return {
                                    status: 'success', data: order
                                };
                            }else{
                                return {
                                    statusCode: 404,
                                    body: {status: 'fail', message: 'Order not found'}
                                };
                            }

                        }
                    },
                    delete: {
                        authRequired: false,
                        action: function() {
                            console.log("delete Order");
                            console.log(this.urlParams.id);
                            if (Orders.remove({RQ_ID: this.urlParams.id})) {
                                console.log("--Order deleted--");

                                return {status: 'success', data: {message: "Order " + this.urlParams.id + " removed"}};
                            }
                            console.log("--Order not found--");

                            return {
                                statusCode: 404,
                                body: {status: 'fail', message: 'Order not found'}
                            };
                        }
                    },
                    put: {
                        authRequired: false,
                        action: function() {
                            console.log("update Order");
                            console.log(this.urlParams.id);
                            if (Orders.update({RQ_ID: this.urlParams.id}, {$set: this.bodyParams})) {
                                console.log("--Order updated--");
                                return {status: 'success', data: {message: "Order " + this.urlParams.id + " updated"}};
                            }
                            console.log("--Order not found--");

                            return {
                                statusCode: 404,
                                body: {status: 'fail', message: 'Order not found'}
                            };
                        }
                    },
                    post: {
                        authRequired: false,
                        action: function() {
                            console.log("insert or update Order");
                            doc = Orders.findOne({RQ_ID: this.bodyParams.item_id});

                            if (doc) {
                                Orders.update({_id: doc._id}, {$set: this.bodyParams});
                                console.log("--Order updated--");
                                return {status: 'success', data: {message: "Order updated"}};
                            } else {
                                Orders.insert(this.bodyParams);
                                console.log("--Order inserted--");
                                return {status: 'success', data: {message: "Order inserted"}};
                            }

                        }
                    }
                }
            }
        );

        Api.addRoute('reset', {
            get: function() {
                Meteor.call('getOrders');
                return {status: 'success', data: {message: "Orders deleted and inserted"}};
            }
        });

        //Api.addRoute('test/:_id', {
        //    get: function() {
        //            // GET api/articles
        //            console.log("tesing API");
        //            console.log(this.urlParams);
        //            return {status: 'success', data: {message: "api tested"}};
        //        }
        //post: function () {
        //    // POST api/articles
        //},
        //put: function () {
        //    // PUT api/articles
        //},
        //patch: function () {
        //    // PATCH api/articles
        //},
        //delete: function () {
        //    // DELETE api/articles
        //},
        //options: function () {
        //    // OPTIONS api/articles
        //}
        //});
    }

});
