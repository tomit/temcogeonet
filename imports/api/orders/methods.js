import _ from 'underscore';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { HTTP } from 'meteor/http';


import { Orders } from './collection';

export function getActions(userId, orderID, rqID) {
    if (Meteor.isServer) {
        if(!(typeof userId !== undefined && userId != null)) {
            userId = this.userId;
        }
        let user = Meteor.users.findOne(userId);

        let url = 'https://euroclean.be/temconet/site/api/webuser/?username=' + user.profile.id +
            '&action=getActionByRequestId&requestID=' + rqID;

        console.log(url);

        HTTP.get(url, (error, result) => {
            //console.log("getting actions");
            if (!error) {
                const data = result.data;
                if( result.data !== null ) {
                    let actions = [];
                    data.forEach((action) => {
                        actions.push({
                            dttime: action.dttime,
                            AC_TypeID: action.AC_TypeID,
                            PM_Description: action.PM_Description,
                            AC_RequestID: action.AC_RequestID,
                            AC_ID: action.AC_ID,
                            AC_CreatorID: action.AC_CreatorID,
                            AC_CreationTime: action.AC_CreationTime,
                            AC_ExecutionTime: action.AC_ExecutionTime,
                            AC_Comment: action.AC_Comment,
                            AC_BillingTopic: action.AC_BillingTopic,
                            AC_Processed: action.AC_Processed,
                            AC_UpdateTime: action.AC_UpdateTime,
                            AC_UpdateUser: action.AC_UpdateUser,
                            AC_Synchronized: action.AC_Synchronized
                        });
                    });

                    Orders.update(orderID, {$push: {actions: {$each: actions}}});
                }

            }
        });
    }

}

export function getTimeslots(userId, orderID, rqID) {
    if (Meteor.isServer) {
        // Get Timeslots
        if(!(typeof userId !== undefined && userId != null)) {
            userId = this.userId;
        }
        let user = Meteor.users.findOne(userId);

        let url = 'https://euroclean.be/temconet/site/api/webuser/?username=' + user.profile.id +
            '&action=getTimeSlotsFromRequest&requestID=' + rqID;

        console.log(url);

        HTTP.get(url, (error, result) => {
            if (!error) {
                //Fake data for now
                const data = result.data;
                //const data = [
                //    {
                //        TS_ID: timeslot.TS_ID,
                //        TS_ExecutorID: timeslot.TS_ExecutorID,
                //        TS_ExecutionDate: timeslot.TS_ExecutionDate,
                //        TS_StartTime: timeslot.TS_StartTime,
                //        TS_StopTime: timeslot.TS_StopTime,
                //        TS_TicketID: timeslot.TS_TicketID,
                //        TS_ActionID: timeslot.TS_ActionID,
                //        TS_Type: timeslot.TS_Type,
                //        TS_Latitude: timeslot.TS_Latitude,
                //        TS_Longitude: timeslot.TS_Longitude,
                //        TS_IP: timeslot.TS_IP,
                //        TS_DeviceID: timeslot.TS_DeviceID,
                //        TS_Reason: timeslot.TS_Reason
                //    },
                //];
                if( result.data !== null ) {
                    let timeslots = [];
                    data.forEach((timeslot) => {
                        timeslots.push({
                            TS_ID: timeslot.TS_ID,
                            TS_ExecutorID: timeslot.TS_ExecutorID,
                            TS_ExecutionDate: timeslot.TS_ExecutionDate,
                            TS_StartTime: timeslot.TS_StartTime,
                            TS_StopTime: timeslot.TS_StopTime,
                            TS_TicketID: timeslot.TS_TicketID,
                            TS_ActionID: timeslot.TS_ActionID,
                            TS_Type: timeslot.TS_Type,
                            TS_Latitude: timeslot.TS_Latitude,
                            TS_Longitude: timeslot.TS_Longitude,
                            TS_IP: timeslot.TS_IP,
                            TS_DeviceID: timeslot.TS_DeviceID,
                            TS_Reason: timeslot.TS_Reason
                        });
                    });
                    let timeslotsReversed = [];
                    //Define current status
                    if (timeslots.length > 0) {
                        timeslotsReversed = _.extend([], timeslots).reverse();
                    }

                    let lastStatus = timeslotsReversed[0];

                    let status = lastStatus.TS_Type == '3' ? 'pauze' : lastStatus.TS_Type == 4 ? 'hold' : lastStatus.TS_StopTime !== '' ? 'stop' : 'start';

                    Orders.update(orderID, {
                        $set: {status: status},
                        $push: {timeslots: {$each: timeslots} }
                    });
                }
            }
        });
    }

}

export function updateTimeslot(_id, startTime, stopTime, typeNumber, actionID, reasonID) {
    if (Meteor.isServer) {
        Orders.update({_id: _id, "timeslots.TS_StartTime": startTime}, {
            $set: {
                "timeslots.$.TS_StopTime": stopTime,
                "timeslots.$.TS_Type": typeNumber,
                "timeslots.$.TS_ActionID": actionID,
                "timeslots.$.TS_Reason": reasonID
            }
        });
    }

}

export function updateActionComment(_id, actionID, comment) {
    if (Meteor.isServer) {
        Orders.update({_id: _id, "actions.AC_ID": actionID}, {
            $set: {
                "actions.$.AC_Comment": comment
            }
        });
    }

}

export function removeOrders(userId) {
    Orders.remove({owner: userId});
}

export function getOrders(userId = null) {
    if (Meteor.isServer) {
        if(!(typeof userId !== undefined && userId != null)) {
            userId = this.userId;
        }
        console.log("init Orders server");

        //Remove all orders and get new
        Orders.remove({owner: userId});

        let user = Meteor.users.findOne(userId);

        let url = 'https://euroclean.be/temconet/site/api/webuser/?username=' + user.profile.id + '&action=getrequests';
        console.log(url);
        let orderObj = {};
        HTTP.get(url, (error, result) => {
            //console.log(result);
            if (!error) {
                const data = result.data;
                if(data && data.length && data.length > 0) {
                    data.forEach((order) => {
                        orderObj = {
                            RQ_ID: order.RQ_ID,
                            CU_Address: order.CU_Address,
                            CU_Zip: order.CU_Zip,
                            CU_Name: order.CU_Name,
                            CU_City: order.CU_City,
                            description: order.description,
                            SI_Name: order.SI_Name,
                            SI_Address: order.SI_Address,
                            SI_Zip: order.SI_Zip,
                            SI_City: order.SI_City,
                            RQ_ExecutionDate: order.RQ_ExecutionDate,
                            EX_TicketID: order.EX_TicketID,
                            owner: userId,
                            actions: [],
                            timeslots: [],
                            timings: []
                        };

                        let id = Orders.insert(orderObj);

                        Meteor.call('getActions', userId, id, orderObj.RQ_ID);

                        Meteor.call('getTimeslots', userId, id, orderObj.RQ_ID);
                    });
                }

                result = {};
                result.data = {
                    critical: 'ok'
                };
                return result.data;
            }
            else {
                console.log("api error:");
                console.log(error);
                result = {};
                result.data = {
                    critical: 'nok'
                };
                return result.data;
            }
        });
    }
}

Meteor.methods({
    getOrders,
    removeOrders,
    getActions,
    getTimeslots,
    updateTimeslot,
    updateActionComment
});
