import { Meteor } from 'meteor/meteor';
import { Counts } from 'meteor/tmeasday:publish-counts';

import { Orders } from './collection';

if (Meteor.isServer) {
    Meteor.publish('numberOfOrders', function() {
        Counts.publish(this, 'numberOfOrders',Orders.find({owner: this.userId}), {nonReactive: true});
    });

    Meteor.publish('orders', function () {
        //console.log("subscribing");

        return Orders.find({
            owner: this.userId
        });

    });

    Meteor.publish('order', function (orderId) {
        //console.log("subscribing");

        return Orders.find({
            RQ_ID: orderId,
            owner: this.userId
        });

    });

}
