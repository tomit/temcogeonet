import './publish';
import './methods';
export * from './collection';

if(Meteor.isServer) {
    Meteor.setInterval(() => {
        //get all users
        let users = Meteor.users.find({}).fetch();
        for(let user of users) {
            Meteor.call('getOrders', user._id);
        }
    }, 25000)
}
