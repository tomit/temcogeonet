import { Mongo } from 'meteor/mongo';

export const Workdays = new Meteor.Collection('workdays');

Workdays.allow({
    insert: () => {
        return true;
    }
});