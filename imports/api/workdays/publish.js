import { Meteor } from 'meteor/meteor';
import { Counts } from 'meteor/tmeasday:publish-counts';

import { Workdays } from './collection';

if (Meteor.isServer) {
    Meteor.publish('workdays', function () {
        //console.log("subscribing");

        return Workdays.find({});

    });
}
