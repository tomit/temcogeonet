import angular from 'angular';

import { Meteor } from 'meteor/meteor';

import { name as Temcogeo } from '../imports/ui/components/temcogeo/temcogeo';

function onReady() {
    angular.bootstrap(document, [
        Temcogeo
    ], {
        strictDi: true
    });
}

if (Meteor.isCordova) {
    angular.element(document).on('deviceready', onReady);
} else {
    angular.element(document).ready(onReady);
}
